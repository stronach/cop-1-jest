import _ from "lodash"

// Truthiness

test("zero", () => {
  const n = 0
  expect(n).not.toBeNull()
  expect(n).toBeDefined()
  expect(n).not.toBeUndefined()
  expect(n).not.toBeTruthy()
  expect(n).toBeFalsy()
})

test("null", () => {
  const n = null
  expect(n)
  expect(n).toBeNull()
  expect(n).toBeDefined()
  expect(n).not.toBeUndefined()
  expect(n).not.toBeTruthy()
  expect(n).toBeFalsy()
})

//Numbers

test("two plus two", () => {
  const n = 2 + 2
  expect(n).toBeGreaterThan(3)
  expect(n).toBeGreaterThanOrEqual(3.5)
  expect(n).toBeLessThan(5)
  expect(n).toBeLessThanOrEqual(4.5)

  // toBe and toEqual are equivalent for integers
  expect(n).toBe(4)
  expect(n).toEqual(4)
})

test("add floating point", () => {
  expect(0.1 + 0.2)
  // .toBe(0.3) // this won't work because of rounding error
    .toBeCloseTo(0.3)
})

// built in jest function `toMatch`
test("has a or z", () => {
  expect("abcdefghijklmn").toMatch(/[az]/)
})

// custom `toInclude` from 'jest-extended'
test("includes `foo`", () => {
  expect("the big fox jumped over the foo dog").toInclude("foo")
})


// Arrays/iterables
const shoppingList = [
  "diapers",
  "kleenex",
  "trash bags",
  "paper towels",
  "beer"
]

describe("the shopping list", () => {
  test("length equals 5", () => {
    expect(shoppingList).toHaveLength(5)
  })
  test("should contain beer", () => {
    expect(new Set(shoppingList)).toContain("beer")
  })
})

// Object

const obj = {
  id: (Math.random() * 100000),
  created: new Date(),
  name: "name",
  colors: [ "red", "green", "blue" ],
  color: "brown",
  val: 1
}

describe("object", () => {
  test("toEqual", () => {
    expect(obj).toEqual({
      id: expect.any(Number),
      created: expect.any(Date),
      colors: expect.arrayContaining(["red", "green"]),
      name: "name",
      val: 1,
      color: expect.stringMatching(/red|green|brown/)
      // color: {
      //   asymmetricMatch: color => _.includes(["red", "green", "brown" ], color)
      // }
    })
  })
  test("toMatchObject", () => {
    expect(obj).toMatchObject({
      id: expect.any(Number),
      name: "name",
      val: 1
    })
  })
})

// Asymmetric

const isFooOrNumber = {
  asymmetricMatch: actual => actual === "foo" || typeof actual === "number"
}
describe("isFooOrNumber when", () => {
  test("null", () => {
    expect(null).not.toEqual(isFooOrNumber)
  })
  test("foo", () => {
    expect("foo").toEqual(isFooOrNumber)
  })
  test("empty string", () => {
    expect("").not.toEqual(isFooOrNumber)
  })
  test("zero (number)", () => {
    expect(0).toEqual(isFooOrNumber)
  })
  test("zero (string)", () => {
    expect("0").not.toEqual(isFooOrNumber)
  })
})
