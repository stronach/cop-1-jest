beforeAll(async () => {
  await Promise.resolve("1")
  // called before all tests are run
})

beforeEach(() => {
  jest.resetAllMocks()
  // called before each test is run
})

afterAll(done => {
  //do something to shutdown
  setTimeout(() => {
    done()
  })
})

describe("a block of tests", () => {
  let someLocalVariable
  beforeAll(() => {
    //called before all the tests in this block are called
  })
  beforeEach(() => {
    someLocalVariable = "foo"
  })
  test("a test run", () => {
    someLocalVariable = "bar"
    expect(someLocalVariable).toBe("bar")
  })
  test("another test", () => {
    expect(someLocalVariable).toBe("foo")
  })
})
