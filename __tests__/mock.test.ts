
describe("mock functions", () => {
  let mockFn

  beforeEach(() => {
    mockFn = jest
      .fn(x => 42 + x)
      .mockName("x => x + 42")
    mockFn(0)
    mockFn(1)
  })

  test("called 2 times (sugar)", () => {
    expect(mockFn).toHaveBeenCalledTimes(2)
  })
  test("called at least once with 0 (sugar)", () => {
    expect(mockFn).toHaveBeenCalledWith(0)
  })
  test("called at least once with 1 (sugar)", () => {
    expect(mockFn).toHaveBeenCalledWith(1)
  })
  test("returned 42 at least once (sugar)", () => {
    expect(mockFn).toHaveReturnedWith(42)
  })
  test("returned 43 at least once (sugar)", () => {
    expect(mockFn).toHaveReturnedWith(43)
  })
  test("called 1st time with 0 (sugar)", () => {
    // The first argument of the first call to the function was 0
    expect(mockFn).toHaveBeenNthCalledWith(1, 0)
  })
  test("called 2nd time with 1 (sugar)", () => {
    expect(mockFn).toHaveBeenNthCalledWith(2, 1)
  })
  test("returned 42 1st time (sugar)", () => {
   // The return value of the first call to the function was 42
    expect(mockFn).toHaveNthReturnedWith(1, 42)
  })
  test("returned 43 2nd time (sugar)", () => {
    // The return value of the second call to the function was 43
    expect(mockFn).toHaveNthReturnedWith(2, 43)
  })

  test("called 2 times (mock)", () => {
    expect(mockFn.mock.calls.length).toBe(2)
  })
  test("called at least once with 0 (mock)", () => {
    expect(mockFn.mock.calls).toContainEqual([0])
  })
  test("called at least once with 1 (mock)", () => {
    expect(mockFn.mock.calls).toContainEqual([1])
  })
  test("called 1st time with 0 (mock)", () => {
    // The first argument of the first call to the function was 0
    expect(mockFn.mock.calls[0][0]).toBe(0)
  })
  test("called 2nd time with 1 (mock)", () => {
    expect(mockFn.mock.calls[1][0]).toBe(1)
  })
  test("returned 42 1st time (mock)", () => {
    // The return value of the first call to the function was 42
    expect(mockFn.mock.results[0].value).toBe(42)
  })
  test("returned 43 2nd time (mock)", () => {
    // The return value of the second call to the function was 43
    expect(mockFn.mock.results[1].value).toBe(43)
  })
  test("returned 42 at least once (mock)", () => {
    expect(mockFn.mock.results.map(r => r.value)).toContain(42)
  })
  test("returned 43 at least once (mock)", () => {
    expect(mockFn.mock.results.map(r => r.value)).toContain(43)
  })


  describe("mockReturnValueOnce", () => {
    beforeEach(() => {
      mockFn.mockClear()
      mockFn
        .mockReturnValueOnce(5)
        .mockReturnValueOnce(6)
      mockFn(5)
      mockFn(5)
      mockFn(5)
    })

    test("returned 5 1st time", () => {
      expect(mockFn).toHaveNthReturnedWith(1, 5)
    })
    test("returned 6 2nd time", () => {
      expect(mockFn).toHaveNthReturnedWith(2, 6)
    })
    test("return 47 3rd time", () => {
      expect(mockFn).toHaveNthReturnedWith(3, 47)
    })
  })

  describe("mockReturnValue", () => {
    beforeEach(() => {
      mockFn.mockClear()
      mockFn.mockReturnValue(6)
      mockFn(5)
      mockFn(5)
    })

    test("returned 6 1st time", () => {
      expect(mockFn).toHaveNthReturnedWith(1, 6)
    })
    test("returned 6 2nd time", () => {
      expect(mockFn).toHaveNthReturnedWith(2, 6)
    })
  })

  describe("mockImplementationOnce", () => {
    beforeEach(() => {
      mockFn.mockClear()
      mockFn
        .mockImplementationOnce(a => a * a)
        .mockImplementationOnce( a => {
          throw new Error("something went wrong")
          return 0
        })
        .mockName("x => x * a")
      mockFn(5)
      try {
        mockFn(5)
      } catch (error) {}
      mockFn(5)
    })

    test("returned 5 squared 1st time", () => {
      expect(mockFn).toHaveNthReturnedWith(1, 25)
    })
    test("threw error at least once including string 'something'", () => {
      const errorIndex = mockFn.mock.results.map( r => r.type).indexOf('throw')
      expect(errorIndex).toBeGreaterThanOrEqual(0)
      expect(mockFn.mock.results[errorIndex].value.message).toInclude("something")
    })
    test("threw error 2nd time with message including 'something'", () => {
      expect(mockFn.mock.results[1].type).toBe("throw")
      expect(mockFn.mock.results[1].value.message).toInclude("something")
    })
    test("returned 47 3rd time", () => {
      expect(mockFn).toHaveNthReturnedWith(3, 47)
    })
  })

})

describe("mock async functions", () => {
  let mockFn
  beforeEach(() => {
    mockFn = jest.fn(async a => Promise.resolve(a * 2))
      .mockResolvedValueOnce(Promise.resolve(4))
      .mockRejectedValueOnce(Promise.reject(new Error("bad number")))
    mockFn(1)
    mockFn(1)
    mockFn(1)
  })

  test("resolves to 4 1st time", () => {
    expect(mockFn.mock.results[0].value).resolves.toBe(4)
  })
  test("rejects with error containing 'bad' 2nd time", () => {
    expect(mockFn.mock.results[1].value).rejects.toThrow("bad")
  })
  test("resolves to 2 3rd time", () => {
    expect(mockFn.mock.results[2].value).resolves.toBe(2)
  })

})

describe("spyOn Math.sqrt", () => {
  let spy
  beforeEach(() => {
    spy = jest.spyOn(Math, 'sqrt')

    Math.sqrt(4)
  })

  test("called 1st time with 4", () => {
    expect(spy).toHaveBeenNthCalledWith(1, 4)
  })
  test("returned sqrt 2", () => {
    expect(spy).toHaveNthReturnedWith(1, 2)
  })
})

describe("spyOn console.log", () => {
  let spy
  beforeEach(() => {
    spy = jest.spyOn(console, 'log')

    console.log("a console message")
  })

  test("console log included string with 'message'", () => {
    expect(spy).toHaveBeenCalledWith(expect.stringContaining("message"))
  })
})

import util from "util"
import fs from "fs"
import { mocked } from "ts-jest/utils"
import { jsonFile } from "./fixtures"

jest.mock("fs")

function readJsonFileSync() : any {
  return JSON.parse(fs.readFileSync("filename.json").toString())
}

async function readJsonFileAsync() : Promise<string> {
  return JSON.parse((await util.promisify(fs.readFile)("filename.json")).toString())
}

describe("mocks", () => {

  beforeEach(() => {
    mocked(fs.readFileSync).mockReturnValue(jsonFile)
  })

  test("auto mocked fs.readFileSync has correct value", () => {
    expect(readJsonFileSync())
      .toHaveProperty("foo", "bar")
  })
  test("manual mocked fs.readFile has correct value", () => {
    expect(readJsonFileAsync())
      .resolves
      .toHaveProperty("foo", "bar")
  })
})


describe("setTimeout mocking", () => {
  let value

  function testFn() {
    setTimeout(() => {
      value = 1
      setTimeout(() => {
        value = 2
      }, 1000)
    }, 1000)
  }

  beforeEach(() => {
    expect.hasAssertions()
    testFn()
  })
  beforeAll(() => {
    jest.useFakeTimers()
  })
  afterAll(() => {
    jest.useRealTimers()
  })

  test("runAllTimers", () => {
    jest.runAllTimers()
    expect(value).toBe(2)
  })
  test("runOnlyPendingTimers", () => {
    jest.runOnlyPendingTimers()
    expect(value).toBe(1)
    jest.runOnlyPendingTimers()
    expect(value).toBe(2)
  })
  test("advanceTimersByTime", () => {
    jest.advanceTimersByTime(1000)
    expect(value).toBe(1)
    jest.advanceTimersByTime(1000)
    expect(value).toBe(2)
  })

})
