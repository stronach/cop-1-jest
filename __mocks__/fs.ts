import { jsonFile } from "../__tests__/fixtures"

const fs : any = jest.genMockFromModule("fs")

fs.readFileAsync = jest.fn((filenamne, callback) => {
  callback(null, JSON.parse(jsonFile))
})

export default fs

