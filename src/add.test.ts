import { add } from "./add"

describe("add", () => {
  test("two positive integers", () =>
    expect(add(1, 2)).toEqual(3)
  )
  test("a larger positive integer to a smaller negative integer", () =>
    expect(add(2, -1)).toEqual(1)
  )
})
