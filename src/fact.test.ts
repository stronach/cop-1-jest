

// Test list of values against same

export function factorial(a: number): number {
  if (a === 0 || a === 1) {
    return 1
  } else {
    return a * factorial(a - 1)
  }
}

describe("factorial of", () => {
  test.each([
    [0, 1],
    [1, 1],
    [2, 2],
    [3, 6],
    [4, 24],
    [5, 120],
    [6, 720],
    [7, 5040],
    [8, 40320],
    [9, 362880]
  ])("%i = %i", (a: number, expected: number) => {
    expect(factorial(a)).toBe(expected)
  })
})

// Mocking example
import _ from "lodash"
import { mocked } from "ts-jest/utils"
import { fact } from "./fact"
import { mult } from "./mult"
jest.mock("./mult")

beforeEach(jest.clearAllMocks)

describe("`fact`", () => {

  const factNum = 9
  let addSpy

  beforeEach(() => {
    addSpy = jest.spyOn(require("./add"), "add")
    fact(factNum)
  })
  test("called `add` n - 1 times", () => {
    expect(addSpy).toHaveBeenCalledTimes(8)
  })
  test.each(_.range(1, factNum - 1))("#%i call to `add` had expected value", i => {
    expect(addSpy.mock.calls[i - 1]).toEqual([factNum + 1 - i, -1])
  })

  test.each(_.range(1, factNum - 1))("#%i call to `mult` had expected value", i => {
    expect(mocked(mult).mock.calls[i - 1]).toEqual([i + 1, fact(i)])
  })
})
