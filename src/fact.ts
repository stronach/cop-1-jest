import { add } from "./add"
import { mult } from "./mult"

export function fact(a: number): number {
  if (a === 0 || a === 1) {
    return 1
  } else {
    return mult(a, fact(add(a, -1)))
  }
}
