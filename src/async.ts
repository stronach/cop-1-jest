import util from "util"

export function doSomethingAsCallback(callback: (error: Error | null, result: string | null) => void): void {
  setTimeout(() => {
    callback(null, "Called back!")
  })
}

export function errorAsCallback(callback: (error: Error | null, result: string | null) => void): void {
  setTimeout(() => {
    callback(new Error("Error callback!"), null)
  })
}

export function doSomethingAsPromise(): Promise<string> {
  return new Promise<string>((resolve, reject) => {
    setTimeout(() => {
      resolve("Promised!")
    })
  })
}

export function errorAsPromise(): Promise<string> {
  return new Promise<string>((resolve, reject) => {
    setTimeout(() => {
      reject(new Error("Error promise!"))
    })
  })
}

export async function doSomethingAwait(): Promise<string> {
  await util.promisify(setTimeout)(20)
  return "Waited!"
}

export async function errorAwait(): Promise<string> {
  await util.promisify(setTimeout)(20)
  throw new Error("Error await!")
}

export class Bean {

  public returnFoo(): string {
    return "foo"
  }

  public errorFoo(): string {
    throw new Error("Error foo!")
  }

  public async asyncReturnFoo(): Promise<string> {
    return this.returnFoo()
  }

  public async asyncErrorFoo(): Promise<string> {
    return this.errorFoo()
  }

  public callbackReturnFoo(callback: (error: Error | null, response: string | null) => void) {
    callback(null, this.returnFoo())
  }

  public callbackErrorFoo(callback: (error: Error | null, response: string | null) => void) {
    try {
      this.errorFoo()
    } catch (error) {
      callback(error, null)
    }
  }
}
