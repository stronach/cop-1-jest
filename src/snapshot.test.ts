import { returnComplexIdChangingObject, returnComplexStaticObject } from "./snapshot"

describe("external", () => {
  test("static", () => {
    expect(returnComplexStaticObject()).toMatchSnapshot()
  })

  test("id changing", () => {
    expect(returnComplexIdChangingObject()).toMatchSnapshot({
      id: expect.any(String),
      created: expect.any(Date)
    })
  })
})

describe("inline", () => {
  test("static", () => {
    expect(returnComplexStaticObject()).toMatchInlineSnapshot(`
      Object {
        "created": null,
        "foo": Object {
          "bang": "bash",
          "bar": 1,
          "prefs": Array [
            Object {
              "a_thing": "is here!",
            },
            2,
          ],
        },
        "id": "123457",
        "values": Array [
          1,
          3,
          "five",
        ],
        "yup": "cowboy!",
      }
    `)
  })
  test("id changing", () => {
    expect(returnComplexIdChangingObject()).toMatchInlineSnapshot(
      {
        id: expect.any(String),
        created: expect.any(Date)
      },
      `
            Object {
              "created": Any<Date>,
              "foo": Object {
                "bang": "bash",
                "bar": 1,
                "prefs": Array [
                  Object {
                    "a_thing": "is here!",
                  },
                  2,
                ],
              },
              "id": Any<String>,
              "values": Array [
                1,
                3,
                "five",
              ],
              "yup": "cowboy!",
            }
        `
    )
  })
})
