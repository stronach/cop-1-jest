
export function returnComplexStaticObject(): any {
  return {
    id: "123457",
    foo: {
      bar: 1,
      bang: "bash",
      prefs: [
        {
          a_thing: "is here!"
        },
        2
      ]
    },
    yup: "cowboy!",
    values: [1, 3, "five"],
    created: null
  }
}

export function returnComplexIdChangingObject(): any {
  const obj = returnComplexStaticObject()
  obj.id = (Math.random() * 100000000).toString(10)
  obj.created = new Date()
  return obj
}


