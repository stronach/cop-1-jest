import util from "util"

import {
  Bean,
  doSomethingAsCallback,
  doSomethingAsPromise,
  doSomethingAwait,
  errorAsCallback,
  errorAsPromise,
  errorAwait
} from "./async"

// Callbacks

beforeEach(() => {
  expect.hasAssertions()
})

describe("callback", () => {
  test("returns", done => {
    doSomethingAsCallback((error, result) => {
      expect(error).toBeNull()
      expect(result).toEqual("Called back!")
      done()
    })
  })

  test("errors", done => {
    errorAsCallback((error, result) => {
      expect(error).toHaveProperty("message", expect.stringContaining("callback"))
      done()
    })
  })
})

// Promises

describe("promise", () => {
  test("returns", () => {
    return doSomethingAsPromise()
      .then(result => {
        expect(result).toEqual("Promised!")
      })
  })
  test("errors", () => {
    return errorAsPromise()
      .catch(error => {
        expect(error).toHaveProperty("message", expect.stringContaining("promise"))
      })
  })
})

//Async/await

describe("async/await", () => {
  test("returns", async () => {
    const result = await doSomethingAwait()
    expect(result).toEqual("Waited!")
  })
  test("errors", async () => {
    await errorAwait()
      .catch(error => {
          expect(error).toHaveProperty("message", expect.stringContaining("await"))
        }
      )
  })
})


// Resolves/rejects

describe("resolves/rejects", () => {
  test("resolves callback", () => {
    return expect(util.promisify(doSomethingAsCallback)())
      .resolves
      .toEqual("Called back!")
  })
  test("error callback", () => {
    return expect(util.promisify(errorAsCallback)())
      .rejects
      .toThrow("callback")
  })
  test("resolves promise", () => {
    return expect(doSomethingAsPromise())
      .resolves
      .toEqual("Promised!")
  })
  test("error promise", () => {
    return expect(errorAsPromise())
      .rejects
      .toThrow("promise")
  })
  test("resolves async/await", () => {
    return expect(doSomethingAwait())
      .resolves
      .toEqual("Waited!")
  })
  test("error async/await", () => {
    return expect(errorAwait())
      .rejects
      .toThrow("await")
  })
})

describe("bean tests", () => {
  let bean
  beforeEach(() => {
    bean = new Bean()
  })

  test("bean async resolves to return foo", () => {
    return expect(bean.asyncReturnFoo())
      .resolves.toEqual("foo")
  })

  test("bean async rejects foo", () => {
    return expect(bean.asyncErrorFoo())
      .rejects.toThrow("foo")
  })

  test("bean callback resolves to return foo", () => {
    return expect(util.promisify(bean.callbackReturnFoo).bind(bean)())
      .resolves
      .toEqual("foo")
  })

  test("bean callback errors to throw foo", () => {
    return expect(util.promisify(bean.callbackErrorFoo).bind(bean)())
      .rejects
      .toThrow("foo")
  })
})
