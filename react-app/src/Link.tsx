import React from 'react';

enum LinkStatus {
  HOVERED = "hovered",
  NORMAL = "normal"
}

interface LinkState {
  class: string
}

interface LinkProperties {
  page: string
  children: any
}

export default class Link extends React.Component<LinkProperties,LinkState> {
  constructor(props:Readonly<LinkProperties>) {
    super(props);

    this._onMouseEnter = this._onMouseEnter.bind(this);
    this._onMouseLeave = this._onMouseLeave.bind(this);

    this.state = {
      class: LinkStatus.NORMAL,
    };
  }

  _onMouseEnter() {
    this.setState({class: LinkStatus.HOVERED});
  }

  _onMouseLeave() {
    this.setState({class: LinkStatus.NORMAL});
  }

  render() {
    return (
      <a
        className={this.state.class}
        href={this.props.page || '#'}
        onMouseEnter={this._onMouseEnter}
        onMouseLeave={this._onMouseLeave}
      >
        {this.props.children}
      </a>
    );
  }
}
