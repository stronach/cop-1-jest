module.exports = {
  preset: 'ts-jest',
  testEnvironment: 'node',
  setupFilesAfterEnv: ["./setupTests.js"],
  testPathIgnorePatterns: [ "dist", "fixtures.ts", "react-app"],
  modulePathIgnorePatterns: [ "dist", "react-app"]
}
